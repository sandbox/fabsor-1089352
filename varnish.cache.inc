<?php

/**
 * Varnish cache implementation.
 *
 * This cache implementation does nothing when trying to save data, but it can
 * be used to issue purges against the Varnish module.
 */
class DrupalVarnishCache implements DrupalCacheInterface {
  protected $bin;

  function __construct($bin) {
    $this->bin = $bin;
  }

  function get($cid) {
    return FALSE;
  }

  function getMultiple(&$cids) {
    return array();  
  }

  function set($cid, $data, $expire = CACHE_PERMANENT) {}

  function clear($cid = NULL, $wildcard = FALSE) {
    // Get our endpoints.
    $servers = varnish_get_all_servers();
    $cache_lifetime = variable_get('cache_lifetime', 0);
    $cache_flush = variable_get('cache_flush_' . $this->bin, 0);
    $host = _varnish_get_host();
    if (empty($cid) && $cache_flush == 0) {
      // This is the first request to clear the cache, start a timer.
      variable_set('cache_flush_' . $this->bin, REQUEST_TIME);
    }
    foreach ($servers as $server) {
      if (REQUEST_TIME > ($cache_flush + $cache_lifetime)) {
        varnish_create_connection($server)->purgeAll($host);
      }
    }
  }

  function isEmpty() {
    // @todo
    // We can probably determine that but it's doesn't feel that important right now.
    // Also, we might not want to take the time to connect to varnish to determine
    // it.
    return FALSE;
  }
}
