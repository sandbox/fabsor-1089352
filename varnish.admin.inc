<?php

/**
 * @file
 * varnish.admin.inc
 *
 * Administrative functions for Varnish integration.
 */

/**
 * List all available servers.
 */
function varnish_admin_servers() {
  $servers = varnish_get_all_servers();
  $rows = array();
  foreach ($servers as $server) {
    $rows[] = array(
      $server['name'],
      $server['host'],
      $server['port'],
      l(t('Delete'), 'admin/config/development/varnish/delete/' . $server['name']) . ' | ' .
      l(t('Edit'), 'admin/config/development/varnish/edit/' . $server['name'])
    );
  }
  return theme('table', array('header' => array(t('Name'), t('Host'), t('Port'), t('Operations')), 'rows' => $rows));
}

function varnish_server_form($form, &$form_state, $server = NULL) {
  $form = array();
  if (!extension_loaded('sockets')) {
    drupal_set_message(t('<a href="http://php.net/manual/en/sockets.installation.php">PHP Sockets extension</a> not enabled. Varnish terminal communication configuration skipped.'), 'error');
  }

  $form['name'] = array(
    '#type' => 'machine_name',
    '#machine_name' => array(
      'exists' => 'varnish_server_load',
    ),
    '#title' => t('Name'),
    '#description' => t('A unique name for this server.'),
    '#default_value' => isset($server['name']) ? $server['name'] : '',
    '#disabled' => isset($server['name']),
  );

  // Begin socket-dependent configuration.  
  $form['legacy'] = array(
    '#type' => 'checkbox',
    '#title' => t('Varnish Legacy Mode'),
    '#default_value' => isset($server['legacy']) ? $server['legacy'] : '127.0.0.1',
    '#description' => t('Check this if you use a version of varnish lower than 2.1'),
  );

  $form['host'] = array(
    '#type' => 'textfield',
    '#title' => t('Host'),
    '#default_value' => isset($server['host']) ? $server['host'] : '127.0.0.1',
    '#required' => TRUE,    
    '#description' => t('Set this to the server IP or hostname that varnish runs on (e.g. 127.0.0.1)'),
  );

  $form['port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port'),
    '#default_value' => isset($server['port']) ? $server['port'] : '6082',
    '#description' => t('Set this to the port the varnish terminal runs on. Default port is 6082'),
  );

  $form['control_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Varnish Control Key'),
    '#default_value' => variable_get('varnish_control_key', ''),
    '#description' => t('Optional: if you have established a secret key for control terminal access, please put it here.'),
  );

  $form['socket_timeout'] = array(
   '#type' => 'textfield',
   '#title' => t('Varnish connection timeout (milliseconds)'),
   '#default_value' => variable_get('varnish_socket_timeout', VARNISH_DEFAULT_TIMETOUT),
   '#description' => t('If Varnish is running on a different server, you may need to increase this value.'),
   '#required' => TRUE,
  );

  /*
  // Check status
  $form['status'] = array(
    '#type' => 'item',
    '#title' => t('Status'),
    '#value' => theme('varnish_status', varnish_get_status()),
  );
  */
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save varnish server'),
    '#weight' => 40,
  );
  return $form;
  
}

function varnish_server_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['socket_timeout']) || $form_state['values']['socket_timeout'] < 0) {
    form_set_error('varnish_socket_timeout', t('Varnish connection timeout must be a positive number.'));
  }
  else {
    $form_state['values']['socket_timeout'] = (int) $form_state['values']['socket_timeout'];
  }
}

function varnish_server_form_submit($form, &$form_state) {
  $server = array();
  $server['legacy'] = $form_state['values']['legacy'];
  $server['name'] = $form_state['values']['name'];
  $server['host'] = $form_state['values']['host'];
  $server['port'] = $form_state['values']['port'];
  $server['control_key'] = $form_state['values']['control_key'];
  $server['socket_timeout'] = $form_state['values']['socket_timeout'];
  varnish_server_save($server);
  $form_state['redirect'] = 'admin/config/development/varnish';
}

function varnish_delete_server_form($form, &$form_state, $server) {
  $form_state['server'] = $server;
  return confirm_form($form, t('Are you sure you want to delete the varnish server %server?',
    array('%server' => $server['name'])), 'admin/config/development/varnish');
}

function varnish_delete_server_form_submit($form, &$form_state) {
  $server = $form_state['server'];
  varnish_server_delete($server['name']);
  $form_state['redirect'] = 'admin/config/development/varnish';
}


/**
 * Menu callback for varnish admin settings.
 */
function varnish_admin_reports_page() {
  // connect to varnish and do a full status report
  $status = _varnish_terminal_run(array('stats'));
  $output = '';
  foreach ($status as $terminal => $stat) {
    $output .= '<pre>'. $stat['stats']['msg'] .'</pre>';
  }
  return $output;
}
