<?php

/**
 * A Varnish exception.
 */
class VarnishException extends Exception {}

/**
 * This interface needs to be implemented by any Varnish connection class
 * that you want to define.
 * This is the minimal functionality that must be provided in order for the
 * Varnish connection to be used with the cache implementation.
 */
interface VarnishConnectionInterface {
  function connect();
  function disconnect();
  function purge($host, $pattern);
}

/**
 * This class is meant to be used for connection to a Varnish terminal
 * and issue commands to it.
 */
class VarnishConnection implements VarnishConnectionInterface {
  protected $socket;
  protected $host;
  protected $port;
  protected $secret;
  protected $legacy;
  protected $connection;

  /**
   * Instantiate the Varnish terminal connection class.
   * @param string $host
   *   The hostname of the Varnish server.
   * @param int $port
   *   The port that the Varnish terminal listens to.
   * @param boolean $legacy
   *   Set this to true if you want to deal with Varnish 2.0 servers.
   * @param string $secret 
   *   The secret key needed to connect to the server.
   */
  function __construct($host, $port, $legacy = FALSE, $secret = NULL) {
    $this->host = $host;
    $this->port = $port;
    $this->legacy = $legacy;
    $this->secret = $secret;
  }
  
  /**
   * Make sure the socket is closed when this connection is terminated.
   */
  function __destruct() {
    $this->disconnect();
  }

  /**
   * Disconnect from a Varnish server.
   */
  function disconnect() {
    if (isset($this->socket)) {
      socket_close($this->socket);
    }
  }

  /**
   * Connect to the varnish terminal using the credentials provided in
   * the constructor.
   * @throws VarnishException
   *  if something goes wrong while connecting.
   */
  function connect() {
    $socket = socket_create(AF_INET, SOCK_STREAM, getprotobyname('tcp'));
    socket_set_option($socket, SOL_SOCKET, SO_SNDTIMEO, array('sec' => $seconds, 'usec' => $milliseconds));
    socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, array('sec' => $seconds, 'usec' => $milliseconds));
    if (@!socket_connect($socket, $this->host, $this->port)) {
      // @todo Throw exception here.
      $error = socket_last_error($socket);
      throw new VarnishException(socket_strerror($error), $error);
    }
    if (!$legacy) {
      $status = _varnish_read_socket($socket);
      // Do we need to authenticate?
      if ($status['code'] == 107 ) { // Require authentication
        if ($this->secret) {
          throw new VarnishException(t('This Varnish terminal requires authentication, but no secret has been provided'), 107);
        }
        $challenge = substr($status['msg'], 0, 32);
        $pack = $challenge ."\x0A". $this->secret ."\x0A". $challenge ."\x0A";
        $key = hash('sha256', $pack);
        socket_write($client, "auth $key\n");
        $status = _varnish_read_socket($socket);
        if ($status['code'] != 200) {
          throw new VarnishException($status['msg'], $status['code']);
        }
      }
    }
    $this->socket = $socket;
  }

  /**
   * Get the host we are connected to.
   * @return string the hostname
   */
  function getHost() {
    return $this->host;
  }

  /**
   * Get the port we are connecting to.
   * @return int
   *   The port that is being used.
   */
  function getPort() {
    return $this->port;
  }

  /**
   * Purge data.
   * @param string $host
   *   the host you want to purge.
   * @param string $pattern 
   *   the pattern you want to match.
   */
  function purge($host, $pattern) {
    $this->executeCommand("purge req.http.host ~ $host && req.url ~ ^$pattern"); 
  }

  /**
   * Helper function to purge all pages on a host.
   * @param type $host 
   */
  function purgeAll($host) {
    $path = base_path();
    _varnish_terminal_run("purge req.http.host ~ $host && req.url ~ ^$path");
  }

  /**
   * Get a list of available VCL configurations.
   * @return array 
   *   an array of vcl keyed by their name. Each row has the following:
   *   - active if this is the active vcl configuration
   *   - name the name of the vcl.
   */  
  function vcl_list() {
    $data = array();
    $result = $this->executeCommand('vcl.list');
    // Lets deal with each row independently.
    $rows = explode('\n', $result['data']);
    foreach ($rows as $row) {
      list($name, $other) = explode('\t',  $row);
      $matches = array();
      preg_match("/(available|active)\t([0-9]+)(.*)/", $row, $matches);
      $data[$matches[3]] = array('active' => $matches[1] == 'active', 'name' =>  $matches[3]);
    }
    return $data;
  }

  /**
   * Load a VCL file from a path on the server.
   * @throws VarnishException
   *   if the command was not executed successfully.
   * @param string $vcl
   *   The vcl file to use.
   * @return string
   *   The status as it was returned by the Varnish terminal.
   */
  function vcl_load($path) {
    $result = $this->executeCommand("vcl.load $path");
    return $result['status'];
  }

  /**
   * Tell Varnish to use a particular VCL file.
   * @throws VarnishException
   *   if the command was not executed successfully.
   * @param string $vcl
   *   The vcl file to use.
   * @return string
   *   The status as it was returned by the Varnish terminal.
   */
  function vcl_use($vcl) {
   $result = $this->executeCommand("vcl.use $vcl");
   return $result['status'];
  }

  /**
   * Start the varnish server.
   * @throws VarnishException
   *   if the command was not executed successfully.
   * @return string
   *   The status as it was returned by the Varnish terminal.
   */
  function start() {
    $result = $this->executeCommand("start");
    return $result['status'];
  }

  /**
   * Stop the varnish server.
   * @throws VarnishException
   *   if the command was not executed successfully.   
   * @return string
   *   the status as it was returned from the Varnish terminal.
   */  
  function stop() {
    $result = $this->executeCommand('stop');
    return $result['status'];
  }

  /**
   * Discard a VCL configuration.
   * @throws VarnishException
   *   if the command was not executed successfully.
   * @param string $vcl
   *   the name of the vcl configuration.
   * @return string
   *   the status code as it was returned from the Varnish terminal.
   */
  function vcl_discard($vcl) {
   $result = $this->executeCommand("vcl.discard $vcl");
   return $result['status'];
  }

  /**
   * Get some statistics from varnish.
   * @todo separate this information into an array.
   * @return string
   *   data about varnish.
   */  
  function stats() {
    $result = $this->executeCommand('stats');
    return $result['msg'];
  }

  /**
   * Get the current status.
   * @return string
   *   The current status, e.g. running or stopped.
   */
  function status() {
    $result = $this->executeCommand('status');
    return $result['msg'];
  }

  /**
   * Execute a varnish command.
   * @throws VarnishException
   *   If the varnish server doesn't answer positively to the command.
   * @param type $client
   * @param type $command
   * @return array status
   *   An array with the following keys:
   *    - msg The message from the varnish server.
   *    - code The code returned from the server.
   */
  private function executeCommand($command) {
    // Send command and get response.
    $result = socket_write($this->socket, "$command\n");
    $status = _varnish_read_socket($client);
    if ($status['code'] != 200) {
      throw new VarnishException($status['msg'], $status['code']);
    }
    else {
      // Successful connection.
      return $status;
    }
  }
  
  /**
   * Low-level socket read function.
   * @throws VarnishException
   *   If something goes wrong with the socket connection.
   * @param int $retry 
   *   How many times to retry on "Temporarily unavailable" errors.
   */
  private function read_socket($retry = 2) {
    // status and length info is always 13 characters. 
    $header = socket_read($this->socket, 13, PHP_BINARY_READ);
    if ($header == FALSE) {
      $error = socket_last_error();
      // 35 = socket-unavailable, so it might be blocked from our write.
      // This is an acceptable place to retry.
      if ($error == 35 && $retry > 0) {
        return $this->read_socket($this->socket, $retry-1);
      }
      else {
        throw new VarnishException('Socket error: ' . socket_strerror($error), $code);
      }
    }
    $msg_len = (int) substr($header, 4, 6) + 1;
    $status = array(
      'code' => substr($header, 0, 3),
      'msg' => socket_read($this->socket, $msg_len, PHP_BINARY_READ)
    );
    return $status;
  }
}
